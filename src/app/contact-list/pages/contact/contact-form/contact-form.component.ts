import { Component, OnInit } from '@angular/core'
import {ContactListService} from '../../../common/services/contact-list/contact-list.service'
import {ActivatedRoute, Router} from '@angular/router'
import {Contact, IPhoneNumber} from '../../../common/models/Contact.models'
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from '../../../common/constants/routes'

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./../contact.component.scss']
})
export class ContactFormComponent implements OnInit {
  contact: Contact

  contactForm: FormGroup

  private buildContactForm(): FormGroup {
    return this.formBuilder.group({
      name: ['', [Validators.required]],
      image: [''],
      favourite: [false],
      email: ['', [Validators.email]],
      numbers: this.formBuilder.array([])
    })
  }

  private initContactForm(): void {
    this.contactForm = this.buildContactForm()
    if (this.contact) {
      if (this.contact.numbers && this.contact.numbers.length) {
        this.contact.numbers.forEach(() => this.addPhoneNumber())
      }
      this.contactForm.patchValue(this.contact)
    }
  }

  constructor(
    private contactService: ContactListService,
    private activeRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.activeRoute.params.subscribe(({id}) => {
      if (id) {
        this.contact = this.contactService.getContact(Number(id))
      }
    })
  }

  get phoneNumbers(): FormArray {
    return this.contactForm.controls.numbers as FormArray
  }

  addPhoneNumber(): void {
    this.phoneNumbers.push(this.formBuilder.group({
      label: ['', [Validators.required]],
      number: ['', [Validators.required, Validators.pattern('\\+*[0-9 ]+')]]
    }))
  }

  removePhoneNumber(index: number, control: any): void {
    this.phoneNumbers.removeAt(index)
  }

  onFileChange(event): void {
    const reader = new FileReader()

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files
      reader.readAsDataURL(file)

      reader.onload = () => {
        this.contactForm.patchValue({
          image: reader.result
        })
      }
    }
  }

  ngOnInit(): void {
    this.initContactForm()
  }

  save(): void {
    if (this.contactForm.valid) {
      if (this.contact) {
        this.contactService.editContact(this.contact.id, this.contactForm.value)
        this.router.navigate([`${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${this.contact.id}`])
      } else {
        this.contactService.addContact(this.contactForm.value)
        this.router.navigate([`${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.list}`])
      }
    }
  }

}
