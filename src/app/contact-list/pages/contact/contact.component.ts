import { Component, OnInit } from '@angular/core'
import {ContactListService} from '../../common/services/contact-list/contact-list.service'
import {ActivatedRoute} from '@angular/router'
import {Contact} from '../../common/models/Contact.models'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from '../../common/constants/routes'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contact: Contact
  backRoute = `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.list}`

  constructor(
    private contactService: ContactListService,
    private activeRoute: ActivatedRoute
  ) {
    this.activeRoute.params.subscribe(({id}) => {
      this.contact = this.contactService.getContact(Number(id))
    })
  }

  ngOnInit(): void {
  }

}
