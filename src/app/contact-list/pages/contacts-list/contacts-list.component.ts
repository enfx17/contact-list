import {Component, Input, OnInit} from '@angular/core'
import {CONTACT_LIST_MENU} from '../../common/constants/menu'
import {Contact} from '../../common/models/Contact.models'
import {ContactListService} from '../../common/services/contact-list/contact-list.service'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from '../../common/constants/routes'

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit {
  menu = CONTACT_LIST_MENU
  searchInput = ''

  contactList: Contact[] = []

  addContactRoute = `/${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.add}`

  @Input() favorites: boolean

  private searchMatch(contact: Contact): boolean {
    return contact.name.toLowerCase().includes(this.searchInput.toLowerCase())
  }

  constructor(private contactsService: ContactListService) { }

  get contacts(): Contact[] {
    return this.contactList.filter(
      (contact: Contact) => this.favorites ? contact.favourite && this.searchMatch(contact) : this.searchMatch(contact)
    )
  }

  private fetchContacts(): void {
    this.contactList = this.contactsService.getContactList()
  }

  ngOnInit(): void {
    this.fetchContacts()
  }

  deleteContact(contact: Contact): void {
    const result = confirm(`Are you sure you want to delete ${contact.name} from your contacts?`)
    if (result) {
      this.contactsService.deleteContact(contact)
      this.fetchContacts()
    }
  }

}
