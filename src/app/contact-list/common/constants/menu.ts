import {IMenuItem} from '../models/MenuItem.models'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from './routes'

export const CONTACT_LIST_MENU: IMenuItem[] = [
  {
    path: `/${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.list}`,
    name: 'All contacts'
  },
  {
    path: `/${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.favourites}`,
    name: 'My Favourites'
  }
]
