export const CONTACT_LIST_ROUTE_FRAGMENTS = {
  root: 'contacts',
  list: 'list',
  favourites: 'favourites',
  add: 'add',
  edit: 'edit'
}
