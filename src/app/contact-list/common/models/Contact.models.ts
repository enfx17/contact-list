export interface IContactList {
  list: IContact[]
}

export interface IContact {
  id?: number
  name: string
  image: string
  favourite: boolean
  email: string
  numbers: IPhoneNumber[]
}

export interface IPhoneNumber {
  label: string
  number: string
}

export class Contact implements IContact {
  id: number
  name: string
  image: string
  favourite: boolean
  email: string
  numbers: IPhoneNumber[]

  constructor(id: number, contact: IContact) {
    this.id = id
    Object.assign(this, contact)
  }
}
