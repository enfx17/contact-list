import { Injectable } from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Contact, IContact, IContactList} from '../../models/Contact.models'


@Injectable({
  providedIn: 'root'
})
export class ContactListService {

  static LOCAL_STORAGE_KEY = 'contact'

  private _uniqueId = 0

  createContact(contact: IContact): Contact {
    return new Contact(++this._uniqueId, contact)
  }

  // Just an initial set of some data in list
  initService(): any {
    const url = '/assets/contacts/contacts.json'
    if (!this.getContactList().length) {
      return this.httpClient.get<IContactList>(url).toPromise().then((contactList: IContactList) => {
        contactList.list.forEach((contact: IContact) => {
          this.addContact(contact)
        })
      })
      .catch((err) => {

      })
    }
    return
  }

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getContactList(): Contact[] {
    const contacts: IContact[] = []
    Object.keys(localStorage).forEach((key) => {
      if (key.includes(ContactListService.LOCAL_STORAGE_KEY)) {
        contacts.push(JSON.parse(localStorage.getItem(key)))
      }
    })
    return contacts.map((contact: IContact) => new Contact(contact.id, contact))
  }

  getContact(id: number): Contact {
    const contact: Contact = JSON.parse(localStorage.getItem(`${ContactListService.LOCAL_STORAGE_KEY}_${id}`))
    return new Contact(contact.id, contact)
  }

  addContact(contact: IContact): void {
    const newContact = this.createContact(contact)
    localStorage.setItem(`${ContactListService.LOCAL_STORAGE_KEY}_${newContact.id}`, JSON.stringify(newContact))
  }

  editContact(id: number, newContact: IContact): void {
    const oldContact = JSON.parse(localStorage.getItem(`${ContactListService.LOCAL_STORAGE_KEY}_${id}`))
    localStorage.setItem(`${ContactListService.LOCAL_STORAGE_KEY}_${id}`, JSON.stringify(Object.assign(oldContact, newContact)))
  }

  favouriteContact(contact: Contact): void {
    this.editContact(contact.id, Object.assign(contact, { favourite: !contact.favourite}))
  }

  deleteContact(contact: Contact): void {
    localStorage.removeItem(`${ContactListService.LOCAL_STORAGE_KEY}_${contact.id}`)
  }

}
