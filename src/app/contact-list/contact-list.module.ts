import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {ContactListRoutingModule} from './contact-list-routing.module'
import { MenuComponent } from './ui-components/menu/menu.component'
import { ContactCardComponent } from './ui-components/contact-card/contact-card.component'

import { FavoritesListComponent } from './pages/favorites-list/favorites-list.component'
import { ContactsListComponent } from './pages/contacts-list/contacts-list.component'
import { AddButtonComponent } from './ui-components/add-button/add-button.component'
import { ContactFormComponent } from './pages/contact/contact-form/contact-form.component'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { HeartIconComponent } from './ui-components/icons/heart-icon/heart-icon.component'
import { EditIconComponent } from './ui-components/icons/edit-icon/edit-icon.component'
import { DeleteIconComponent } from './ui-components/icons/delete-icon/delete-icon.component'
import { ContactComponent } from './pages/contact/contact.component';
import { SearchIconComponent } from './ui-components/icons/search-icon/search-icon.component';
import { FavouriteBtnComponent } from './ui-components/buttons/favourite-btn/favourite-btn.component';
import { EditBtnComponent } from './ui-components/buttons/edit-btn/edit-btn.component';
import { DeleteBtnComponent } from './ui-components/buttons/delete-btn/delete-btn.component';
import { BackIconComponent } from './ui-components/icons/back-icon/back-icon.component';
import { BackBtnComponent } from './ui-components/buttons/back-btn/back-btn.component';
import { PhotoIconComponent } from './ui-components/icons/photo-icon/photo-icon.component'



@NgModule({
  declarations: [
    MenuComponent,
    ContactCardComponent,
    FavoritesListComponent,
    ContactsListComponent,
    AddButtonComponent,
    ContactFormComponent,
    HeartIconComponent,
    EditIconComponent,
    DeleteIconComponent,
    ContactComponent,
    SearchIconComponent,
    FavouriteBtnComponent,
    EditBtnComponent,
    DeleteBtnComponent,
    BackIconComponent,
    BackBtnComponent,
    PhotoIconComponent
  ],
  imports: [
    ContactListRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ContactListModule { }
