import {Component, Input, OnInit} from '@angular/core'
import {IMenuItem} from '../../common/models/MenuItem.models'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() menu: IMenuItem[]

  constructor() { }

  ngOnInit(): void {
  }

}
