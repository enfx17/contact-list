import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core'
import {Contact} from '../../common/models/Contact.models'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from '../../common/constants/routes'

@Component({
  selector: 'app-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: ['./contact-card.component.scss']
})
export class ContactCardComponent implements OnInit {
  @Input() contact: Contact
  @Output() delete = new EventEmitter<Contact>()

  contactRoute: string

  constructor() { }

  ngOnInit(): void {
    this.contactRoute = `/${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${this.contact.id}`
  }

  deleteContact(): void {
    this.delete.emit(this.contact)
  }

}
