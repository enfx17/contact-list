import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from '../../../common/constants/routes'
import {Contact} from '../../../common/models/Contact.models'

@Component({
  selector: 'app-edit-btn',
  templateUrl: './edit-btn.component.html',
  styleUrls: ['./edit-btn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditBtnComponent implements OnInit {
  @Input() contact: Contact
  editRoute: string

  constructor() { }

  ngOnInit(): void {
    this.editRoute = this.editRoute = `/${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${this.contact.id}/${CONTACT_LIST_ROUTE_FRAGMENTS.edit}`
  }

}
