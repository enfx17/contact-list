import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core'
import {Contact} from '../../../common/models/Contact.models'
import {ContactListService} from '../../../common/services/contact-list/contact-list.service'

@Component({
  selector: 'app-favourite-btn',
  templateUrl: './favourite-btn.component.html',
  styleUrls: ['./favourite-btn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FavouriteBtnComponent implements OnInit {
  @Input() contact: Contact

  constructor(private contactService: ContactListService) { }

  ngOnInit(): void {
  }

  toggleFavourite(): void {
    this.contactService.favouriteContact(this.contact)
  }

}
