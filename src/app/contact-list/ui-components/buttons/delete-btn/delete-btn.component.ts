import {Component, OnInit, ViewEncapsulation} from '@angular/core'

@Component({
  selector: 'app-delete-btn',
  templateUrl: './delete-btn.component.html',
  styleUrls: ['./delete-btn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DeleteBtnComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
