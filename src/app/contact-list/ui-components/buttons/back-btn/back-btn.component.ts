import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core'
import {Location} from '@angular/common'
import {Router} from '@angular/router'

@Component({
  selector: 'app-back-btn',
  templateUrl: './back-btn.component.html',
  styleUrls: ['./back-btn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BackBtnComponent implements OnInit {
  @Input() backRoute: string

  constructor(
    private location: Location,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  goBack(): void {
    if (this.backRoute) {
      this.router.navigate([this.backRoute])
    } else {
      this.location.back()
    }
  }
}
