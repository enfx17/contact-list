import { NgModule } from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {CONTACT_LIST_ROUTE_FRAGMENTS} from './common/constants/routes'
import {ContactsListComponent} from './pages/contacts-list/contacts-list.component'
import {FavoritesListComponent} from './pages/favorites-list/favorites-list.component'
import {ContactFormComponent} from './pages/contact/contact-form/contact-form.component'
import {ContactComponent} from './pages/contact/contact.component'


const CONTACT_LIST_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.list}`
  },
  {
    path: `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.list}`,
    component: ContactsListComponent
  },
  {
    path: `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.favourites}`,
    component: FavoritesListComponent
  },
  {
    path: `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/${CONTACT_LIST_ROUTE_FRAGMENTS.add}`,
    component: ContactFormComponent
  },
  {
    path: `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/:id`,
    component: ContactComponent
  },
  {
    path: `${CONTACT_LIST_ROUTE_FRAGMENTS.root}/:id/${CONTACT_LIST_ROUTE_FRAGMENTS.edit}`,
    component: ContactFormComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(CONTACT_LIST_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class ContactListRoutingModule { }
