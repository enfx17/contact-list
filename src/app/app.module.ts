import { BrowserModule } from '@angular/platform-browser'
import {APP_INITIALIZER, NgModule} from '@angular/core'

import { AppComponent } from './app.component'
import {ContactListModule} from './contact-list/contact-list.module'
import {RouterModule, Routes} from '@angular/router'
import {HttpClientModule} from '@angular/common/http'
import {ContactListService} from './contact-list/common/services/contact-list/contact-list.service'

export function contactsFactory(contactListService: ContactListService): () => void {
  return () => contactListService.initService()
}

const APP_ROUTES: Routes = [
  {
    pathMatch: 'full',
    path: '',
    loadChildren: () => import('./contact-list/contact-list.module').then(m => m.ContactListModule)
  },
  {path: '**', redirectTo: ''}
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ContactListModule,
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),
    HttpClientModule
  ],
  providers: [
    ContactListService,
    {
      provide: APP_INITIALIZER,
      useFactory: contactsFactory,
      deps: [ContactListService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
